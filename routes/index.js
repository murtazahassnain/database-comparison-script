var express = require("express");
var router = express.Router();
var path = require("path");

router.get("/result", function (req, res) {
  res.sendFile(path.resolve("result.html"));
});

module.exports = router;
