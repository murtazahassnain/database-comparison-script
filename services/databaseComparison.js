const e = require("express");
const dbConn1 = require("../models/index");
const dbConn2 = require("../models/index_second");
const fs = require("fs");
const ip = require("ip");
const path = require("path");

const getCount = async () => {
  try {
    let arrayOfRowsCount = [];
    let table_names = await dbConn1.sequelize.query(`select table_name as table
              from information_schema.tables
              where table_schema = 'public' order by table_name`);

    console.log("Fetched tables from Database of Connection 1...");

    let arrayOfRowsCount1 = [];
    let table_names_2 = await dbConn2.sequelize
      .query(`select table_name as table
              from information_schema.tables
              where table_schema = 'public' order by table_name`);

    console.log("Fetched tables from Database of Connection 2...");

    console.log(
      "Calculating Row Count of all Tables in Database Connection 1..."
    );
    for (let i = 0; i < table_names[0].length; i++) {
      let no_of_rows = await dbConn1.sequelize.query(
        `select count(*) as rows from "${table_names[0][i].table}";`
      );
      arrayOfRowsCount.push(no_of_rows[0][0]);
    }
    let arrayOfTablesCount = table_names[0];
    console.log(
      "Successfully calculated Row Count of all Tables in Database Connection 1..."
    );

    console.log(
      "Calculating Row Count of all Tables in Database Connection 2..."
    );
    for (let i = 0; i < table_names_2[0].length; i++) {
      let no_of_rows = await dbConn2.sequelize.query(
        `select count(*) as rows from "${table_names_2[0][i].table}";`
      );
      arrayOfRowsCount1.push(no_of_rows[0][0]);
    }
    let arrayOfTablesCount1 = table_names_2[0];
    console.log(
      "Successfully calculated Row Count of all Tables in Database Connection 2..."
    );

    let connection_1 = [];
    for (let i = 0; i < arrayOfTablesCount.length; i++) {
      const tableName = arrayOfTablesCount[i].table;
      const rowCount = arrayOfRowsCount[i].rows;
      const jsonObject = { tableName, rowCount };
      connection_1.push(jsonObject);
    }

    let connection_2 = [];
    for (let i = 0; i < arrayOfTablesCount1.length; i++) {
      const tableName = arrayOfTablesCount1[i].table;
      const rowCount = arrayOfRowsCount1[i].rows;
      const jsonObject = { tableName, rowCount };
      connection_2.push(jsonObject);
    }

    return [connection_1, connection_2];
  } catch (error) {
    throw error;
  }
};

const spotChecking = async () => {
  try {
    let [tables1, tables2] = await getCount();
    let resultCommon = [],
      resultUnique = [],
      common = [],
      notCommon = [],
      connection_1,
      connection_2;

    console.log(
      "Starting comparison of both Databases on",
      new Date().toLocaleString("ur-PK", { timeZone: "Asia/Karachi" })
    );

    for (let table = 0; table < tables1.length; table++) {
      if (tables2.some((el) => el.tableName === tables1[table].tableName)) {
        console.log("Spot Checking on Table number: ", table + 1);
        common.push(tables1[table].tableName);
        tables2
          .map((object) => object.tableName)
          .indexOf(tables1[table].tableName);

        let index = tables2
          .map((object) => object.tableName)
          .indexOf(tables1[table].tableName);

        if (tables1[table].rowCount < 20000) {
          let page = 200;
          let result1 = [],
            result2 = [];

          let progressOK = true;
          for (let i = 0; i < tables1[table].rowCount; i = i + page) {
            connection_1 = await dbConn1.sequelize.query(
              `SELECT * FROM "${tables1[table].tableName}" OFFSET ${i} LIMIT ${page}`
            );
            result1.push(connection_1[0]);

            connection_2 = await dbConn2.sequelize.query(
              `SELECT * FROM "${tables2[index].tableName}" OFFSET ${i} LIMIT ${page}`
            );
            result2.push(connection_2[0]);

            if (JSON.stringify(result1) === JSON.stringify(result2)) {
              result1 = [];
              result2 = [];
            } else {
              progressOK = false;
              break;
            }
          }

          progressOK === true
            ? resultCommon.push({
                table: tables1[table].tableName,
                similar: "True",
                rowCount1: tables1[table].rowCount,
                rowCount2: tables2[index].rowCount,
              })
            : resultCommon.push({
                table: tables1[table].tableName,
                similar: "False",
                rowCount1: tables1[table].rowCount,
                rowCount2: tables2[index].rowCount,
              });
        } else {
          let columns = await dbConn1.sequelize.query(
            `SELECT column_name FROM information_schema.columns WHERE table_schema = 'public' AND table_name = '${tables1[table].tableName}'`
          );

          let idExists = columns[0].some((el) => el.column_name === "id");

          if (idExists) {
            connection_1 = await dbConn1.sequelize.query(
              `(SELECT * FROM "${tables1[table].tableName}" order by id LIMIT 100) union (SELECT * FROM "${tables1[table].tableName}" order by id desc LIMIT 100) order by id`
            );
            connection_2 = await dbConn2.sequelize.query(
              `(SELECT * FROM "${tables2[index].tableName}" order by id LIMIT 100) union (SELECT * FROM "${tables2[index].tableName}" order by id desc LIMIT 100) order by id`
            );
          } else {
            let colNotId = columns[0][0].column_name;
            connection_1 = await dbConn1.sequelize.query(
              `(SELECT * FROM "${tables1[table].tableName}" order by ${colNotId} LIMIT 100) union (SELECT * FROM "${tables1[table].tableName}" order by ${colNotId} desc LIMIT 100) order by ${colNotId}`
            );
            connection_2 = await dbConn2.sequelize.query(
              `(SELECT * FROM "${tables2[index].tableName}" order by ${colNotId} LIMIT 100) union (SELECT * FROM "${tables2[index].tableName}" order by ${colNotId} desc LIMIT 100) order by ${colNotId}`
            );
          }

          JSON.stringify(connection_1[0]) == JSON.stringify(connection_2[0])
            ? resultCommon.push({
                table: tables1[table].tableName,
                similar: "True",
                rowCount1: tables1[table].rowCount,
                rowCount2: tables2[index].rowCount,
              })
            : resultCommon.push({
                table: tables1[table].tableName,
                similar: "False",
                rowCount1: tables1[table].rowCount,
                rowCount2: tables2[index].rowCount,
              });
        }
      } else {
        notCommon.push(tables1[table].tableName);
        resultUnique.push({
          table: tables1[table].tableName,
          similar: "False",
          rowCount1: tables1[table].rowCount,
          rowCount2: tables1[table].rowCount,
        });
      }
    }

    for (let table = 0; table < tables2.length; table++) {
      if (!common.includes(tables2[table].tableName)) {
        notCommon.push(tables2[table].tableName);
        resultUnique.push({
          table: tables2[table].tableName,
          similar: "False",
          rowCount1: tables2[table].rowCount,
          rowCount2: tables2[table].rowCount,
        });
      }
    }

    console.log(
      "Done comparing both Databases on",
      new Date().toLocaleString("ur-PK", { timeZone: "Asia/Karachi" })
    );

    return [resultCommon, resultUnique];
  } catch (error) {
    throw error;
  }
};

module.exports = {
  getCount,
  spotChecking,
};
