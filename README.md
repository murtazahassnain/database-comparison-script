<h1>Welcome to Database Comparison Script!</h1>

<h3>Following are a few instructions on how to use the script:</h3>

<h5>1. How to Run:</h5>Run <b>"npm install"</b> to install the packages required.<br>Open Terminal in the directory of the script and simply run <b>"npm start"</b>.

<h5>2. How to configure your database in the script:</h5> Create a .env file in the root directory of the project and enter your database credentials. Example:

<b>NODE*ENV=</b>"development"<br>
<b>PORT=</b> <port number e.g 3000, 5000><br>
<b>DATABASE_NAME=</b> <db_name><br>
<b>DATABASE_USERNAME=</b> <user_name><br>
<b>DATABASE_PASSWORD=</b> <password*><br>
<b>DATABASE_HOST=</b> <ip_address><br>
<b>DATABASE_PORT=</b> <Database_port_number><br>
<b>DATABASE_DIALECT=</b> <e.g postgres>

<b>DATABASE_NAME_2=</b> <db_name (same as first connection)><br>
<b>DATABASE_USERNAME_2=</b> <user_name (same as first connection)><br>
<b>DATABASE_PASSWORD_2=</b> <password (same as first connection)><br>
<b>DATABASE_HOST_2=</b> <ip_address of second connection><br>
<b>DATABASE_PORT_2=</b> <Database_port_number of second connection><br>

<h5>3. How to View Output:</h5> Go to <b>localhost:port_number/result</b> for viewing the comparison result between both databases.<br><br>

<b>Note:</b> If the result shows "True", it means every record of that table in both databases is identical and if "False" then atleast one record is different.
