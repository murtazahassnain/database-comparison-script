const { spotChecking } = require("../services/databaseComparison");
const path = require("path");

const fs = require("fs");

const spotCheck = async () => {
  let [result, result2] = await spotChecking();

  fs.writeFile(
    path.join(__dirname, "../public/result.json"),
    JSON.stringify({ result, result2 }),
    (err) => {
      if (err) {
        console.error(err);
      }
    }
  );
};

module.exports = {
  spotCheck,
};
